# Flag

Show your flag!

Controls:
* Pressing petal II (or app button to the right): Next flag
* Pressing petal VIII (or app button to the left): Previous flag

## Available Flags

* Classic Rainbow
* Transgender Flag
* Bisexual Flag
* Pansexual Flag
* Gender Nonbinary Flag
* Lesbian Flag
* Agender Flag
* Asexual Flag
* Genderqueer Flag
* Genderfluid Flag
* Aromantic Flag

## Config File

When exiting, a config file will be written to `/flash/flag.json`.
It will be used to ensure that the same flag is shown again at
the next start.

You can also edit it to configure if the petals should be deactivated
(in case you accidentally trigger those), by setting `use_petals` to
`0`, and to disable the LEDs (eco-mode!) by setting `use_leds` to `0`.

You can also modify `slew_rate` to change the smoothness of the LED animation.

