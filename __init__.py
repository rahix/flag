#
# Show your flag!
# Switch flags with petals II and VIII
#
# (sorry for the hacky code :)
# Douglas 2023-08
#

import st3m.run, random

from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from ctx import Context
import leds
import json

class Configuration:
    def __init__(self) -> None:
        self.scheme: int = 0
        self.use_petals: int = 1
        self.use_leds: int = 1
        self.slew_rate: int = 8

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "scheme" in data and type(data["scheme"]) == int:
            res.scheme = data["scheme"]
        if "use_petals" in data and type(data["use_petals"]) == int:
            res.use_petals = data["use_petals"]
        if "use_leds" in data and type(data["use_leds"]) == int:
            res.use_leds = data["use_leds"]
        if "slew_rate" in data and type(data["slew_rate"]) == int:
            res.slew_rate = data["slew_rate"]
        return res

    def save(self, path: str) -> None:
        d = {
            "scheme": self.scheme,
            "use_petals": self.use_petals,
            "use_leds": self.use_leds,
            "slew_rate": self.slew_rate,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()


def color_to_rgb(col):
    return [(col >> i & 0xff) / 256 for i in reversed(range(0, 24, 8))]

class Flag(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.t = 0
        self.pos = 0
        self.last_led_update = 0

        # colors taken from https://www.kapwing.com/resources/official-pride-colors-2021-exact-color-codes-for-15-pride-flags/
        self.colors = [
            # classic rainbow flag
            [0xe50000, 0xff8d00, 0xffee00, 0x028121, 0x004cff, 0x770088, ],
            # transgender flag
            [0x5bcffb, 0xf5abb9, 0xffffff, 0xf5abb9, 0x5bcffb, 0x000000, ],
            # bisexual flag
            [0xd60270, 0xd60270, 0x9b4f96, 0x0038a8, 0x0038a8, 0x000000, ],
            # pansexual flag
            [0xff1c8d, 0xffd700, 0x1ab3ff, ],
            # nonbin
            [0xfcf431, 0xfcfcfc, 0x9d59d2, 0x282828, ],
            # lesbian
            [0xd62800, 0xff9b56, 0xffffff, 0xd462a6, 0xa40062, ],
            # agender
            [0x000000, 0xbababa, 0xffffff, 0xbaf484, 0xffffff, 0xbababa, 0x000000, ],
            # asex
            [0x000000, 0xa4a4a4, 0xffffff, 0x810081, ],
            # genderqueer
            [0xb57fdd, 0xffffff, 0x49821e, ],
            # genderfluid
            [0xfe76a2, 0xffffff, 0xbf12d7, 0x000000, 0x303cbe, ],
            # aromantic
            [0x3ba740, 0xa8d47a, 0xffffff, 0xababab, 0x000000, ],
        ]

        self._filename = "/flash/flag.json"
        self._config = Configuration.load(self._filename)
        self.scheme = self._config.scheme

        leds.set_auto_update(True)
        leds.set_slew_rate(self._config.slew_rate)
        leds.set_all_rgb(0,0,0)

    def on_exit(self) -> None:
        self._config.scheme = self.scheme
        self._config.save(self._filename)
        leds.set_auto_update(False)
        leds.set_slew_rate(255)
        leds.set_all_rgb(0,0,0)
        leds.update()


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.t += delta_ms

        petals = self.input.captouch.petals
        btns = self.input.buttons.app

        if (self._config.use_petals and petals[2].whole.pressed) or btns.right.pressed:
            self.scheme = (self.scheme + 1) % len(self.colors)
            self.t = 0
            self.last_led_update = -1000
        elif (self._config.use_petals and petals[8].whole.pressed) or btns.left.pressed:
            self.scheme = (self.scheme - 1) % len(self.colors)
            self.t = 0
            self.last_led_update = -1000

        self.pos = (self.t / 20) % 240


    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False
        nr_colors = len(self.colors[self.scheme])
        stripe = 240 / nr_colors
        idx = int(self.pos / stripe)
        yoff = self.pos % stripe

        # center of screen is [0, 0]
        # x goes right, y goes down -> bottom left is [-120, +120]

        for i in range(nr_colors+1):
            col = self.colors[self.scheme][(idx-i)%nr_colors]
            ctx.rectangle(-120, +120 - yoff - stripe*i, 240, stripe)
            ctx.rgb(*color_to_rgb(col))
            ctx.fill()

        # we don't need to update the LEDs every few ms :)
        if self._config.use_leds and (
                not self.last_led_update or self.t - self.last_led_update > 50
                ):
            for i in range(40):
                led_idx = int(nr_colors * (i / 40 + self.pos/240))
                leds.set_rgb(i, *color_to_rgb(self.colors[self.scheme][led_idx % nr_colors]))
            self.last_led_update = self.t


# For running with `mpremote run`:
if __name__ == "__main__":
    st3m.run.run_view(Flag(ApplicationContext()))
